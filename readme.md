Quality Assurance Engineering Workshop
======================================

This repository is a base for a two day workshop focused on building User Interface automated tests.


Pre-requisites
--------------

    Java
    Maven
    
    
How to work with this repository
--------------------------------

This repository has a number of branches for each step in the workshop named `step-xxx`. The `readme.md` file in each step is 
updated to include instructions on how to move on to the next step.

# Step 1

Create the base project using a maven archetype:
    
    mvn archetype:generate -DgroupId=com.mindera.qae -DartifactId=workshop-qae
    
    


